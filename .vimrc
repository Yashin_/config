" Author: Patric Plattner
" Description: This is my personal vimrc. You can use it if you want *shrug*
"-------------------------------------------------------------------------------
" Set 'nocompatible' to ward off unexpected things that your distro might
" have made, as well as sanely reset options when re-sourcing .vimrc
set nocompatible

"-------------------------------------------------------------------------------
" PLUGIN SECTION
" Install vundle with following command:
" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
"Here are all the plugins

" Plugin 'itchyny/landscape.vim'

" added nerdtree
" File explorer in vim.
Plugin 'scrooloose/nerdtree'

" added fugitive
" Git client
Plugin 'tpope/vim-fugitive'

" added asyncrun (dependency for cmake)
Plugin 'skywind3000/asyncrun.vim'

" added vim-cmake
" obviously needs cmake to be installed
" And well... its a cmake client
Plugin 'vhdirk/vim-cmake'

" added YouCompleteMe
" needs python3, python3-dev and cmake to be installed. after :PluginInstall,
" go to ~/.vim/bundle/YouCompleteMe and run 
" 'python3 install.py --clang-completer --rust-completer'
" This plugin is for autocomplete
Plugin 'Valloric/YouCompleteMe'

" added fzf (dependency for fzf.vim)
Plugin 'junegunn/fzf'

" added fzf.vim used for finding files
" For quickly searching and finding files
Plugin 'junegunn/fzf.vim'

" added lightline
" Alternative statusbar
" Plugin 'itchyny/lightline.vim'

" added vim-eunuch
" Some Unix file operations like chmod mv cp and so on
Plugin 'tpope/vim-eunuch'

" added vim-gitgutter
" Adds git diffs to side of file
Plugin 'airblade/vim-gitgutter'

" added vim-addon-mw-utils (dependency for vim-snipmate)
Plugin 'MarcWeber/vim-addon-mw-utils'

" added tlib_vim (dependency for vim-snipmate)
Plugin 'tomtom/tlib_vim'

" added vim-snipmate
" Well, its a snippet engine
Plugin 'garbas/vim-snipmate'

" added delimitMate.vom
" Automatically places closing brackets quotes and so on
Plugin 'delimitMate.vim'

" added taglist.vim
" Simple sourcecode browser, find classes and so on
Plugin 'taglist.vim'

" added vim-airline
" Alternative statusbar
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" Plugin 'itchyny/lightline.vim'

" added vimsence
" Discord Rich Presence
Plugin 'anned20/vimsence'

Plugin 'iamcco/markdown-preview.vim'

call vundle#end()


let g:airline#extensions#tabline#enabled = 1


" END OF PLUGINS SECTION
"-------------------------------------------------------------------------------

" Attempt to determine the type of a file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype indent plugin on
 
" Enable syntax highlighting
syntax on

"-------------------------------------------------------------------------------
" One such option is the 'hidden' option, which allows you to re-use the same
" window and switch from an unsaved buffer without saving it first. Also allows
" you to keep an undo history for multiple files when re-using the same window
" in this way. Note that using persistent undo also lets you undo in multiple
" files even in the same window, but is less efficient and is actually designed
" for keeping undo history after closing Vim entirely. Vim will complain if you
" try to quit without saving, and swap files will keep you safe if your computer
" crashes.
set hidden

" Better command-line completion
set wildmenu

" Show partial commands in the last line of the screen
set showcmd

" Highlight searches (use <C-L> to temporarily turn off highlighting; see the
" mapping of <C-L> below)
set hlsearch
"-------------------------------------------------------------------------------
" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase

" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start

" When opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent

" Stop certain movements from always going to the first character of a line.
" While this behaviour deviates from that of Vi, it does what most users
" coming from other editors would expect.
set nostartofline

" Display the cursor position on the last line of the screen or in the status
" line of a window
set ruler

" Always display the status line, even if only one window is displayed
set laststatus=2

" Instead of failing a command because of unsaved changes, instead raise a
" dialogue asking if you wish to save changed files.
set confirm

" Use visual bell instead of beeping when doing something wrong
set visualbell

" Setting shell to bash, so I can use fish os-wide and not mess up vim
set shell=/usr/bin/bash

" Enable use of the mouse for all modes
set mouse=a

" Set the command window height to 2 lines, to avoid many cases of having to
" "press <Enter> to continue"
set cmdheight=2

" Display line numbers on the left
set relativenumber

" Quickly time out on keycodes, but never time out on mappings
set notimeout ttimeout ttimeoutlen=200

" Use <F11> to toggle between 'paste' and 'nopaste'
set pastetoggle=<F11>

"-------------------------------------------------------------------------------
" Indentation options {{{1
"
" Indentation settings according to personal preference.

" Indentation settings for using 4 spaces instead of tabs.
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
"-------------------------------------------------------------------------------
" Marking lines that pass 80 chars
if exists('+colorcolumn')
    set colorcolumn=80
else
    au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80.\+', -1)
endif
